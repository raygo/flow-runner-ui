/**
 * All right reserved@Raygo 2022~2030
 */

package com.raygo.runner.stepeditor;

import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

/**
 * 主界面JAVAFX程序
 *
 * @author Raygo
 * @since 2022年3月1日
 */
public class HomeStage extends Application {
    /**
     * 启动
     *
     * @param args 参数
     */
    public static void start(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getResource("/fxml/MainView.fxml"));
        Parent root;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Scene scene = new Scene(root, 1280, 720);
        scene.getStylesheets().add(getResource("/css/jfoenix-components.css").toExternalForm());
        primaryStage.setTitle("步骤编辑器");
        primaryStage.setMaximized(true);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * 获取资源URL
     *
     * @param path 路径
     * @return URL
     */
    protected URL getResource(String path) {
        String newPath = path.startsWith("/") ? path : "/" + path;
        URL url = getClass().getResource(newPath);
        if (url != null) {
            return url;
        }
        return getClass().getResource("/resources" + newPath);
    }
}

/**
 * All right reserved@Raygo 2022~2030
 */

package com.raygo.runner.stepeditor;

/**
 * 主程序
 *
 * @author Raygo
 * @since 2022年3月1日
 */
public class Main {
    /**
     * 入口
     *
     * @param args
     */
    public static void main(String[] args) {
        HomeStage.start(args);
    }
}

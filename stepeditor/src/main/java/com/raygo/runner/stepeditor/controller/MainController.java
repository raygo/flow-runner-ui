/**
 * All right reserved@Raygo 2022~2030
 */

package com.raygo.runner.stepeditor.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.TreeView;
import javafx.scene.input.ContextMenuEvent;

import java.net.URL;
import java.util.ResourceBundle;

import com.pixelduke.control.Ribbon;
import com.pixelduke.control.ribbon.RibbonTab;
import com.raygo.runner.core.command.CommandInfo;

/**
 * 主界面控制器
 *
 * @author Raygo
 * @since 2022年8月7日
 */
public class MainController implements Initializable {
    @FXML
    private Ribbon ribbon;

    @FXML
    private TreeView<CommandInfo> leftTreeView;

    @FXML
    private TreeTableView<?> midTreeTable;

    @FXML
    private SplitPane splitPane;

    @FXML
    private TextArea taCmdDesc;

    @FXML
    private TextArea taCmdParams;

    @FXML
    private CheckBox cbShowMenuBar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initLeftPaneView();
    }

    /**
     * 初始化左侧视图
     */
    private void initLeftPaneView() {        
        TreeItem<CommandInfo> rootTreeItem = new TreeItem<>(new CommandInfo("命令集", ""));
        rootTreeItem.setExpanded(true);
        for (int i = 0; i < 5; i++) {
            TreeItem<CommandInfo> childItem = new TreeItem<>(new CommandInfo("Set" + i, ""));
            for (int j = 0; j < 10; j++) {
                childItem.getChildren().add(new TreeItem<CommandInfo>(new CommandInfo("Command" + j, "命令")));
            }
            rootTreeItem.getChildren().add(childItem);
        }
        leftTreeView.setRoot(rootTreeItem);
        leftTreeView.setShowRoot(false);
        leftTreeView.setEditable(false);
    }

    @FXML
    public void onFileOpen(ActionEvent event) {
        taCmdDesc.setText("onFileOpen");
    }

    @FXML
    public void onFileNew(ActionEvent event) {
        taCmdDesc.setText("onFileNew");
    }

    @FXML
    public void onFileSave(ActionEvent event) {
        taCmdDesc.setText("onFileSave");
    }

    @FXML
    public void onFileSaveAs(ActionEvent event) {
        taCmdDesc.setText("onFileSaveAs");
    }

    @FXML
    public void onRibbonContext(ContextMenuEvent event) {
        taCmdParams.setText("onRibbonClick");
    }

    @FXML
    public void onShowMenuBar(ActionEvent event) {
        ObservableList<RibbonTab> tabs = ribbon.getTabs();
        if (cbShowMenuBar.isSelected()) {
            for (RibbonTab tab : tabs) {
                tab.showTab();
            }
        } else {
            for (RibbonTab tab : tabs) {
                tab.hideTab();
            }
        }
    }
}
